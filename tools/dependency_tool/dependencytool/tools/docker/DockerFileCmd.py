from .DockerFile import DockerFile
from .DockerRun import DockerRun
from .Docker import Docker

class DockerFileCmd():
    """ User commond handler to generate dockerfiles taking into account command line options """
    def help(self):
        return "generate a DockerFile"

    def options(self, subparser):
        subparser.add_argument("-b", "--build", action='store_true', help="build spin components only")

    def fn(self, args):

        platform = args.get_platform()
        if platform is None:
            return 1

        # early spin check
        args.get_spins()

        if not args.build():
            filters = { "build" : False  }
        else:
            filters = {}

        build_cmds = platform.install_commands(args.get_spin_deps(filters), spin="build")

        docker = args.project().tool("Docker", platform)
        name = "project: " + args.project().name()
        if not args.spin() is None:
            name += " spin: " + args.spin()
        docker_file = DockerFile(docker = docker, name = name)
        docker_file.add(build_cmds)
        docker_file.write()

