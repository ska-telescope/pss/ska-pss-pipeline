from .CiCache import CiCache
from .CiStage import CiStage
from dependencytool.Commands import Command, InitCommand, Procedure
from dependencytool.tools.docker.DockerRun import DockerRun
from dependencytool.platforms.AlpineLinux import AlpineLinux
from dependencytool.Dependency import Dependency

class CiOciCached(object):
    """ An OciImageStragey that does not share updated images between stages
    """
    def __init__(self, platform, build_config, project, app_name):
        self._platform = platform
        self._config = build_config

        # -- internal vars
        self._name = self._config.name() + "_" + platform.name()
        docker_cache_base = "docker_images"
        docker_cache = docker_cache_base + "/" + self._name
        self._docker_filename = docker_cache + "/" + self._name + "_docker.tar.gz"
        cache_key = "binaries-$CI_COMMIT_REF_SLUG"
        self._cache = CiCache( key=cache_key, paths = [ docker_cache ])

        # TODO make configurable
        ci_docker_platform = AlpineLinux()
        ci_docker_tool = project.tool("Docker", ci_docker_platform)
        self._docker_tool = ci_docker_tool
        self._docker_exe = ci_docker_tool.docker_exe()
        self._ci_packages = ci_docker_platform.install_commands(
                                   [ Dependency("python")
                                   , Dependency("gzip")
                                   ])
        self._tag_name = self._name.lower() + ":$CI_COMMIT_REF_SLUG"
        python_exe = ci_docker_platform.executable("python", Dependency("python"))
        self._app_exe = python_exe + " " + app_name

        self._load_docker_image = Procedure("load_docker_image")
        self._load_docker_image.add([ InitCommand(self._docker_exe
                                    + " load -q -i " + self._docker_filename
                                    )
                                    ])
        self._services = [ "docker:dind" ]

    """ return a list of CiStage objects required
    """
    def stage_configure(self):
        tag_name = self._name.lower() + ":$CI_COMMIT_REF_SLUG"
        container_stage = self._name + "_build_container"
        spin_name = self._config.spin().name()
        ci_container_stage = CiStage(name=container_stage
                , stage="configure"
                , cache=self._cache
                , image=self._docker_tool.image()
                , services=self._services
                , before_script = Procedure( commands= [ self._ci_packages
                    , InitCommand("mkdir -p " + self._cache.paths()[0])
                    , Command( self._docker_exe + " system df" )
                    , Command( "df -h" )
                    ]
                )
                , procedure = Procedure( commands= [ Command(self._app_exe
                        + " -p " + self._platform.id()
                        + " -s " + spin_name
                        + " dockerfile -b"
                        + " | "
                        + self._docker_exe
                        + " build"
                        + " -t " + tag_name 
                        + " -")
                    , Command(self._docker_exe
                        + " save " + tag_name
                        + " | gzip > " + self._docker_filename)
                ]
                )
        )
        return [ ci_container_stage ]

    def before_script(self):
        return self._load_docker_image

    def stage(self, stage, name, procedure, artifacts, work_dir):
        docker_run = DockerRun(self._docker_tool, options="--rm -t -w "  + work_dir + " -v `pwd`:" + work_dir + " " + self._tag_name)
        proc = Procedure(name, commands=[ Command(self._docker_exe + " images")
                                        , Command(docker_run.command_line(procedure))
                                        ])
        ci_stage = CiStage( name=name
                          , before_script=self.before_script()
                          , image=self._docker_tool.image()
                          , stage=stage
                          , services=self._services
                          , procedure=proc
                          , cache=self._cache
                          , artifacts=artifacts
                          )
        return ci_stage
