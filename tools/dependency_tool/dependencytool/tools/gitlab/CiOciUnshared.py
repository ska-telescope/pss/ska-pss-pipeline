from .CiStage import CiStage

class CiOciUnshared(object):
    """ An OciImageStrategy that does not share updated images between stages
    """
    def __init__(self, platform, build_config, project, app_name):
        self._docker_tool = project.tool("Docker", platform)
        self._build_config = build_config
        self._platform = platform

    def stage_configure(self):
        return []

    def before_script(self):
        return self._platform.install_commands(self._build_config.dependencies(), spin="build")

    def stage(self, stage, name, procedure, artifacts, work_dir):
        ci_stage = CiStage( name=name
                          , stage=stage
                          , image=self._docker_tool.image()
                          , before_script=self.before_script()
                          , procedure=procedure
                          , artifacts=artifacts
                          )
        return ci_stage
