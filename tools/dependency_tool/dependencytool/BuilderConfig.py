from .SpinSpec import SpinSpec
from .StageGroup import StageGroup, StageAll, StageFilter, is_stage_group
from .Context import Context

class StageFilterConfig(object):
    def __init__(self, filters, default_group=None, primary_spin_group=None):
        """
            Class allows the mapping of specific spins and the build stages that should be
            performed for that spin.

            It allows you to provide named filters consisting of groups of stages
            either as an explcit list or using group operators to specify rules.

            These filters can then be associated with specific spins.

        """
        self._group_config = {}
        self._stage_group_map = {}
        for gp_name in filters:
            gp = filters[gp_name]
            if isinstance(gp, list):
                gp = StageGroup( gp )
            self._group_config[gp_name] = gp

        # if not specifed, the default group will build every stage
        if not default_group:
            self._default_group = StageAll()
        else:
            if not default_group in self._group_config:
                raise RuntimeError("unknown group " + name + " specified as default group")
            self._default_group = self._group_config[default_group]
        assert is_stage_group(self._default_group)

        if primary_spin_group is None:
            self._primary_spin_group = self._default_group
        else:
            if isinstance(primary_spin_group, str):
                if not primary_spin_group in self._group_config:
                    raise RuntimeError("unknown group " + primary_spin_group + " specified as primary_spin_group")
                self._primary_spin_group = self._group_config[primary_spin_group]
            else:
                self._primary_spin_group = primary_spin_group
        assert is_stage_group(self._primary_spin_group)


    def set_spin_group(self, spin, group):
        # -- determine the user specifed group object
        if isinstance(group, str):
            # case where group is a name
            # first check it is a defined group
            if not group in self._group_config:
                raise RuntimeError("unknown group " + group + " specified")
            gp = self._group_config[group]
        else:
            gp = group

        if isinstance(spin, str):
            spin = SpinSpec(spin)
        else:
            if not isinstance(spin, SpinSpec):
                raise TypeError("Expecting a SpinSpec or a string")

        self._stage_group_map[str(spin)] = gp

    def stage_group(self, ctx):
        """
            check for explicit stage group specification for the ctx provided
            if defined it will return this group, or the default group otherwise
        """
        explicit_gp_name = ctx.args().stage_filter()
        if explicit_gp_name:
            if not explicit_gp_name in self._group_config:
                print("Known filters:")
                for filter in self._group_config:
                    print("  " + filter)
                raise RuntimeError("unknown group filter \"" + explicit_gp_name + "\" requested")
            return self._group_config[explicit_gp_name]

        # check for an explcit spin -> filter map
        spin_spec = ctx.spin_spec()
        if spin_spec != SpinSpec(""):
            if not isinstance(spin_spec, SpinSpec):
                raise TypeError("Expecting a SpinSpec, got :" + str(type(spin_spec)))

            if str(spin_spec) in self._stage_group_map:
                return self._stage_group_map[str(spin_spec)]

            if len(spin_spec) == 1:
                return self._primary_spin_group

        return self._default_group

class BuilderContext(object):
    """
        Internal Implementation class
        Its function is to provide information (such as the spin spec)
        from either the builder object or from the Context
        as appropriate.
    """
    def __init__(self, build_config, ctx):
        if not isinstance(ctx, Context):
            raise TypeError("Expecting a Context")
        self._build_config = build_config
        self._ctx = ctx

    def tool_args(self, tool_name):
        """
            check if any special arguments need to be passed
            to a specific tool in the tool chain
        """
        return self._ctx.tool_args(tool_name)

    def args(self):
        """
            return the list of user arguments passed
        """
        return self._ctx.args()

    def spin_spec(self):
        """
            return the spin spec required
        """
        spin_spec = self._ctx.spin_spec()
        if spin_spec:
            return spin_spec
        return self._build_config.spin().spin_spec()

class BuilderConfig(object):
    def __init__(self, builder, config={}, stage_groups={}):
        """
            builder the type of Builder required
                    Currently only CMakeBuilder is available
            config: the configuration parameters for this builder type
            stage_groups : the configuration StageFilterConfig object to allow filtering by spin
        """
        self._type = builder
        self._config = config
        self._stage_config = stage_groups
        if not stage_groups:
            self._default_stage_map = StageAll
        self._contexts = {}

    def builder(self, build_config, platform, ctx):
        """
            create a builder of the type specified in the constructor
        """
        ext_ctx = BuilderContext(build_config, ctx)
        builder = self._type(build_config, platform, ext_ctx, self._config)
        self._contexts[id(builder)] = ext_ctx
        return builder

    def stage_filter(self, builder):
        """
            return a stage filter tailored to the configurations settings for the specifed spin
        """
        stage_group = self._stage_config.stage_group(self._contexts[id(builder)])
        return StageFilter(stage_group, builder.stages())
