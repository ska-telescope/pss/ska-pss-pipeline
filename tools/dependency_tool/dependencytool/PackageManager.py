from .Package import Package

class ToolConfig:
    def __init__(self, name, params = {}):
        self._name = name
        self._params = params

    def name(self):
        return self._name

    def copy(self):
        return self._params.copy()

#
# private class to PackageManager
#
class RepoPackage(object):
    def __init__(self, repo, package=Package()):
        self._pkg = package
        self._repo = repo

    def name(self):
        return self._pkg.name()

    def version(self):
        return self._pkg.version()

    def attribute(self, name):
        return self._pkg.attribute(name)

    def repo(self):
        return self._repo

    def __eq__(self, other):
        return self._pkg == other

    def __ne__(self, other):
        return self._pkg != other

    def __str__(self):
        return self._pkg.__str__()

class PackageManager(object):
    """ API for a generic package manager
    """
    def __init__(self, name, package_dict={}, repo_install_cmds=[]):
        self._name = name
        self._packages = { "": RepoPackage(self) }
        self._repo_install_cmds = repo_install_cmds

        for pkg_name in package_dict:
            pkg = package_dict[pkg_name]

            pkg_dict = self._packages.get(pkg_name)
            if pkg_dict is None:
                self._packages[pkg_name] = {}

            if isinstance(pkg, list):
                pkgs = pkg
            else:
                pkgs = [ pkg ]

            for p in pkgs:
                pkg_version = str(p.version())
                if pkg_version not in self._packages[pkg_name]:
                    self._packages[pkg_name][pkg_version] = []
                self._packages[pkg_name][pkg_version].append(RepoPackage(self, p))

    def name(self):
        return self._name

    def package(self, dependency, spin="runtime"):
        name = dependency.name()
        pkg_dict = self._packages.get(name)
        if pkg_dict is None:
            return self._packages[""]
        if str(dependency.version()) in pkg_dict:
            pkgs = pkg_dict.get(str(dependency.version()))
        else:
            # -- anonymous version
            pkgs = pkg_dict.get("")

        # -- look for matching spin
        if pkgs is not None:
            for pkg in pkgs:
                if pkg.attribute("spin") == spin:
                    return pkg;

        # -- no exact match so search all the versions for next best
        for version in pkg_dict:
            pkgs = pkg_dict[version]
            matching = []
            for pkg in pkgs:
                if pkg.version().matches(dependency.version()):
                    if pkg.attribute(spin) == spin:
                        return pkg
                    matching.append(pkg)

            # -- if we are left with only one matching we are done
            if len(matching) == 1:
                return matching[0]

            # -- determine between multiple version matching variants
            #    by selecting the first runtime spin
            for pkg in matching:
                if pkg.attribute("spin") == "runtime":
                    return pkg

        return self._packages[""]

    def packages(self, dependency_list, spin):
        pkgs = []
        for dep in self.dependency_list:
            pkgs.append(self.package(dep, spin))
        return pkgs

    def install_repository(self):
        return self._repo_install_cmds;

