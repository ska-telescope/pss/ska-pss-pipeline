from .BuildStage import BuildStage
from .BuilderConfig import BuilderConfig

class Builder(object):
    """ Internal Wrapper class of all Builder Specialisations
        The builder object passed should support the following methods:
            stages() : return a list of stages to construct the build products
                       as BuildStage objects
    """
    def __init__(self, builder, config={}):
        assert isinstance(config, BuilderConfig)
        self._builder = builder
        self._config = config
        self._stage_filter = self._config.stage_filter(builder=self._builder)


    def stages(self):
        """
            return a list of stages defined by the builder as BuildStage objects
            These will be filtered according to the configuration passed
        """
        if not self._stage_filter:
            return self._builder.stages()

        # -- apply the stage filter
        stages = []
        for stage in self._builder.stages():
            if not self._stage_filter.contains(stage):
                continue
            stages.append(stage)
        return stages

    def build_directory(self):
        """
            return the build directory used by the underlying builder
        """
        return self._builder.build_directory()
