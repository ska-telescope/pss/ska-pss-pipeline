import os
import copy
from .Version import Version
from .Dependency import Dependency
from .DependencyNode import DependencyNode

class DependencyFile:
    def __init__(self, dependencies_file):
        self._dependencies = DependencyNode(self);
        if not isinstance(dependencies_file, str):
            raise("Expecting a filename as a string")
        fh = open(dependencies_file, 'r')
        parse_info = { "indent" : [0]
                     , "node" : self._dependencies
                     , 'last_node' : self._dependencies
                     }
        for full_line in fh:
            parse_info = self._parse_line(parse_info, full_line)

    def root_node(self):
        return self._dependencies

    def dependencies(self):
        return self._dependencies.subdependencies()

    def _parse_line(self, parse_info, full_line):
        line = full_line.lstrip()

        # skip comments
        if line.startswith('#'):
            return parse_info;

        indent=len(full_line) - len(line)
        line = line.rstrip()

        # skip empty lines
        if len(line) == 0:
            return parse_info;

        prev_indent = parse_info["indent"][-1]
        node = parse_info["node"]

        # indents determine the tree structure
        if indent != prev_indent:
            if indent > prev_indent:
                node = parse_info["last_node"]
                parse_info["indent"].append(indent)
            else:
                # -- python does not have a do while loop
                #    so we use this construct
                while True:
                    node = node.parent();
                    parse_info["indent"].pop()
                    if parse_info["indent"][-1] <= indent:
                        break
        else:
            node = parse_info["node"]

        # tokenize the line
        tokens = line.split(' ')
        name = tokens[0]
        tokens.pop(0)
        version = ""
        optional = False
        build = False # marked as a build tool
        if len(tokens) > 0:
            if tokens[-1] == "build":
                build = True
                tokens.pop()

            if len(tokens)>0:
                if tokens[-1] == "optional":
                    optional = True
                    if len(tokens) > 1:
                        version = tokens[0]
                else:
                    version = tokens[0]

        last_node = node.add_dependency(
                            Dependency(name, version, optional, build))
        return { "node": node
               , "indent": parse_info["indent"]
               , "last_node": last_node
               }
