from .Dependency import Dependency

class DependencyNode(Dependency):
    """A single node in a dependency tree hierarchy
       Extends a Dependency object by adding subdependencies
    """
    def __init__(self, parent, dependency = None):
        self._parent = parent
        self._dependencies = []
        if dependency is not None:
            super(DependencyNode, self).copy(dependency)
        else:
            super(DependencyNode, self).__init__("")

    def add_dependency(self, dependency):
        if not isinstance(dependency, Dependency):
            raise TypeError("DependencyNode::add_dependency requires a Dependency object")
        self._dependencies.append(DependencyNode(self, dependency))
        return self._dependencies[-1]

    def subdependencies(self):
        return self._dependencies

    def parent(self):
        return self._parent

    def find(self, dependency):
        """ find all matching dependencies in the tree
            returns a list of matching dependencies
        """
        matches = []
        if super(DependencyNode, self).matches(dependency):
            matches.append(self)

        for dep in self._dependencies:
            matches.extend(self.find(dependency))

        return matches

    def find_first(self, dep_name):
        """ find the first dependency in the tree with the matching name
            None will be returned if there are no matches
        """
        if self.name() == dep_name:
            return self

        for dep in self._dependencies:
            rv = dep.find_first(dep_name)
            if rv is not None:
                return rv
        return None

    def find_with_attribute(self, attribute_filters = {} ):
        """
            returns the immediate subdependencies that match the specified attributes
        """
        required = []
        for dep in self._dependencies:
            if(dep.matches_attributes(attribute_filters)):
                required.append(dep)
        return required;

    def find_with_attribute_recursive(self, attribute_filters = {} ):
        """
            returns all subdependencies as a list that match the specified attributes
        """
        required = []
        for dep in self._dependencies:
            if dep.matches_attributes(attribute_filters):
                required.append(dep)
            required.extend( dep.find_with_attribute_recursive(attribute_filters) )
        return required

    def import_deps(self, other_node):
        """
            import deps from another node into the current tree
            This will overwrite existing dependencies in this tree with the same name
            but will leave in tact all other dependencies, including in subtrees
        """
        if not isinstance(other_node, DependencyNode):
            raise TypeError("DependencyNode::import_deps requires a DependencyNode object")

        # -- filter out any anonymous root nodes
        if not other_node.name():
            for other_dep in other_node.subdependencies():
                self.import_deps(other_dep)
            return

        dep = self.find_first( other_node.name() )
        if dep is None:
            # - import the dependency
            dep = self.add_dependency(other_node)
            dep._dependencies = other_node.subdependencies()
            return

        # - replace the existing node with the new node
        #   overwriting any deps in the subtree too
        dep.copy(other_node)
        for other_dep in other_node.subdependencies():
            dep.import_deps(other_dep)
