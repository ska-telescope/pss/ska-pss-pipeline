from .Dependencies import Dependencies
from .Platforms import Platforms
from .CiCmds import CiCmds
from .Context import Context
from .BuildConfig import build_configurations, build_config_from_spin
from .Builder import Builder
from .DependencyNode import DependencyNode
from .DependencyFile import DependencyFile
from .SpinSpec import SpinSpec

# -------------------------------
# import tool specifications
# -------------------------------
from dependencytool.tools.docker.Docker import Docker

# ----------------------
# Project Description
# ----------------------
class Project(object):
    """ Primary API for Settings and accessing objects tailored to a specific project """

    def __init__(self, name, args, project_config
               , platforms=None # default is to use all known platforms
               ):
        """
            name            : name of the project
            args            : lhe list of args from the command line
                                 args.platform to restrict platforms
                                 args.deps to add extra dependency specifications (as a comma seperated list of filenames)
                                           deps from each file will be added together
            project_config  : A dictionary containing project specific settings. Keys in the dictionary:
                              - builder_config   : a BuilderConfig object specifying the build instructions for the project
                              - dependencies     : a filename, or list of filenames/DependencyNode objects
        """
        self._config = project_config
        self._name = name

        # -- load in dependency requirements for the project
        if isinstance(project_config["dependencies"], str):
            proj_deps = [ project_config["dependencies"] ]
        else:
            proj_deps = project_config["dependencies"]

        if args.deps is not None:
            for token in args.deps.split(","):
                proj_deps.append(token)

        dep_node = DependencyNode(self)
        for dep in proj_deps:
            if isinstance(dep, str):
                # if its a string we assume its a filename for the standard format file
                dep_node.import_deps( DependencyFile(dep).root_node() )
            else:
                dep_node.import_deps( dep )

        self._deps = Dependencies(dep_node)

        self._builder_config = project_config["builder_config"]
        self._configurations = []             # store the build configurations once they have been calculated

        # -- process the platforms for the project
        known_platforms = Platforms();
        self._platforms = {}
        if args.platform is not None:
            name = args.platform.lower()
            try:
                self._platforms[name] = known_platforms.platform(name)
            except KeyError:
                raise NameError("unnown platform requested:" , name)
        else:
            if platforms is None:
                # default is to use all platforms
                self._platforms = known_platforms.platform_dictionary()
            else:
                for plat in platforms:
                    name = plat.lower()
                    try:
                        self._platforms[name] = known_platforms.platform(name)
                    except KeyError:
                        raise NameError("unnown platform requested:" , name)


        # tool extensions
        self._tools = {}
        self._tool_config = {}

        self._ctx = Context(CiCmds(args, self))

    def update_test_conf(self, config):
        """ Prepends project name to spin """
        return {self._name + "_" + key:val for key, val in config.items()}


    def name(self):
        """ returns the name of the project """
        return self._name

    def dependencies(self):
        """ return the Projects Dependecies object
            by defualt this information is takes from the dependencies.txt file
            in the top directory
        """
        assert isinstance( self._deps, Dependencies )
        return self._deps

    def platforms(self):

        """ return the Platform objects that the project is configured for """
        return self._platforms.values()

    def configurations(self):
        """ return the build configurations that are possible for this project and platform
            As well as the basic configuration of all required dependencies, a  configuration
            is generated for each optional dependency. A further configuration with all options activated
            is also generated
        """
        if len(self._configurations) == 0:
            spin_spec = self._ctx.spin_spec()
            if spin_spec != SpinSpec(""):
                # -- context specified by user
                for spin in self._ctx.spins():
                    self._configurations.append(build_config_from_spin(spin, self._name))
            else:
                # -- all possible combos
                self._configurations = build_configurations(self._name, self._deps)
        return self._configurations

    def builder(self, build_config, platform):
        return Builder(builder=self._builder_config.builder(build_config, platform, self._ctx)
                     , config=self._builder_config)

    def tool_config(self, name):
        return self._config[name]

    def find_spins(self, spin_spec):
        return self.dependencies().find_spins(spin_spec);

    def tool(self, tool_name, platform):
        """ to allow extensibility the Project can be assigned various tools configured specifically
            to the project. This function provides access to these tool objects
        """
        lname = tool_name.lower()
        platform_tools = self._tools.get(platform)
        tool = None
        if platform_tools is not None:
            tool = platform_tools.get(tool_name, self._tools.get(lname))
        else:
            self._tools[platform] = {}

        if tool is not None:
            return tool

        # generate a suitable tool object on demand
        tool_class = globals()[tool_name]
        if tool_class is None:
            try:
                __import__(tool_name)
            except:
                raise TypeError("unable to find tool" + tool_class)

        # -- generate the tool configuration from platfrom specific with local project overrides
        platform_config = platform.tool_config(lname)
        if platform_config is None:
            config = {}
        else:
            config = platform_config.copy()

        project_config = self._tool_config.get(lname)
        if project_config is not None:
            config.update(project_config)
        self._tools[platform][lname] = tool_class(platform, config)
        return self._tools[platform][lname]

