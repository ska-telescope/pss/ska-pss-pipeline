from __future__ import print_function
import argparse
import sys

from .Platforms import Platforms
from .Commands import Procedure, Command, InitCommand
from .BuildConfig import BuildConfig, build_config_from_spin
from .CiCmds import CiCmds

# -- import tool specific command interfaces ---
from .tools.cpack.CpackCmd import CpackCmd
from .tools.docker.DockerFileCmd import DockerFileCmd
from .tools.gitlab.GitLabCiCmd import GitLabCiCmd

# ----------------------------
# Definitions of App commands
# ----------------------------

class BuildCmd():
    """ User command handler to generate installation commands taking into account command line options """
    def help(self):
        return "generate commands to build a specific spin for a specifed platform"

    def options(self, subparser):
        subparser.add_argument("-e", "--execute", action='store_true', help="execute the commands instead of printing to screen")

    def fn(self, ci_args):
        platform = ci_args.get_platform()
        if platform is None:
            return 1

        spins = ci_args.get_spins()

        for spin in spins:
            build_config = build_config_from_spin(spin, ci_args.project().name())
            platform_builder = ci_args.project().builder(build_config, platform)

            for build_stage in platform_builder.stages():
                if build_stage.name() != "build":
                    continue
                for cmd in build_stage:
                    if not ci_args.execute():
                        print (str(cmd))
                    else:
                        cmd.execute()
        return 0

class DependencyCmd():
    """ User command handler to list dependencies based on command line arguments """
    def help(self):
        return "list dependencies"

    def options(self, subparser):
        subparser.add_argument("-b", "--build", action='store_true', help="list required components only")

    def fn(self, ci_args):

        spins = ci_args.project().find_spins(ci_args.get_spin_spec())
        if len(spins) == 0:
            raise NameError("unknown spin requested '" + ci_args.spin() + "'")

        indent = "  "
        if not ci_args.build():
            filters = { "build" : False }
        else:
            filters = {}

        for dep in ci_args.get_spin_deps(filters):
            print (indent + dep.name())

        return 0

class InstallCmd():
    """ User command handler to generate installation commands taking into account command line options """
    def help(self):
        return "generate commands to install the required dependencies"

    def options(self, subparser):
        subparser.add_argument( "--no_build", action='store_true', help="exclude the build tools")
        subparser.add_argument("-b", "--build_only", action='store_true', help="only the build tools")
        subparser.add_argument("-e", "--execute", action='store_true', help="execute the commands instead of printing to screen")

    def fn(self, ci_args):
        dep_filter = {}

        use_case = "build"
        if ci_args.args().no_build:
            use_case = "runtime"
            dep_filter = { "build": False }
        elif ci_args.args().build_only:
            dep_filter = { "build": True }

        platform = ci_args.get_platform()
        if platform is None:
            return 1

        """Get the required dependencies"""
        deps = ci_args.get_spin_deps( dep_filter )

        build_cmds = Procedure();
        build_cmds.add( platform.install_commands(deps, use_case))

        for cmd in build_cmds:
            if not ci_args.execute():
                print (str(cmd))
            else:
                cmd.execute()
        return 0

class ListSpinsCmd():
    """ User commond handler to generate list of spins that can be generated for the project """
    def help(self):
        return "generate a list of viable different configurations this project can be built as"

    def options(self, subparser):
        pass

    def fn(self, ci_args):
        for spin in ci_args.project().dependencies().spins():
            print (spin.name())
            pass

class PlatformInfoCmd():
    """ User commond handler to list platform information taking into account command line options """
    def help(self):
        return "list of known platforms"

    def options(self, subparser):
        pass

    def fn(self, ci_args):
        platforms = Platforms()
        for platform in sorted(platforms.platform_names()):
            print (platform)

# ----------------------
# App defintion
# ----------------------

class DeployToolApp(object):
    """ The DeployTool application
        e.g.

        app = DeployToolApp()
        MyProject project(app.args())
        sys.exit(app.execute(project))

    """
    def __init__(self):
        # -- commands
        self._cmds = {  'install'      : InstallCmd()
                , 'build'        : BuildCmd()
                , 'cpack'        : CpackCmd()
                , 'dependencies' : DependencyCmd()
                , 'dockerfile'   : DockerFileCmd()
                , 'gitlab_ci'    : GitLabCiCmd()
                , 'platforms'    : PlatformInfoCmd()
                , 'spins'        : ListSpinsCmd()
               }
        cmd_subparsers={}

        # -- top level options
        pgm_name = "deploytool"
        parser = argparse.ArgumentParser(prog=pgm_name, description="Deployment Utilities")
        parser.add_argument("-sf", "--stage_filter", help="generate the build stages specified by the named stage filter")
        parser.add_argument("-p", "--platform", help="select a specific platform")
        parser.add_argument("-s", "--spin", help="specify a specific spin as a comma seperated list of package names")
        parser.add_argument("-d", "--deps", help="specify a comma seperated list of dependency files")
        parser.add_argument("-a", "--arg", help="specify an argument to be passed directly to the builder")
        parser.add_argument("--tool_<XXX>", type=str
                          , metavar="options for tool XXX"
                          , help="specify command line options to the specified tool (e.g. --tool_cmake=\"-DCXX=clang\")")

        # -- add any user defined tool_XXX type arguments so the parser does not complain
        for i in range(0, len(sys.argv)):
            arg = sys.argv[i]
            if arg.startswith('--tool_'):
                parser.add_argument(arg, action='append')
                # we need to stop argparse from interpretting the value
                # as another option, so we add a character at the beginning
                # we can strip off later
                i = i + 1
                sys.argv[i] = "A" + sys.argv[i]
        subparsers = parser.add_subparsers(dest='cmd', help='sub-command help')
        for cmd in self._cmds.keys():
            cmd_subparsers[cmd] = subparsers.add_parser(cmd, help=self._cmds[cmd].help())
            self._cmds[cmd].options(cmd_subparsers[cmd])
        #print(cmd_subparsers)
        self._args = parser.parse_args()
        self._args.prog = pgm_name

    def args(self):
        return self._args

    def execute(self, project):
        if self._args.cmd is None:
            print("use '" + self._args.prog +  " --help' for help information")
            return 0
        return self._cmds[self._args.cmd].fn(CiCmds(self._args, project))

