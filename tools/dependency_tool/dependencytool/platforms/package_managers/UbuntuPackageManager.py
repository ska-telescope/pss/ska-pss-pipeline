from .AptPackageManager import AptPackageManager
from dependencytool.Package import Package, BuildPackage
from dependencytool.Version import Version
from dependencytool.Commands import Command, InitCommand


class UbuntuCudaPackageManager(AptPackageManager):
    def __init__(self, cuda_version):
        cuda_version = str(cuda_version).replace(".", "-")
        packages = {
           "cuda" : Package("cuda-{}".format(cuda_version))
           ,"cuda-runtime" : Package("cuda-runtime-{}".format(cuda_version))
           ,"cuda-toolkit" : BuildPackage("cuda-toolkit-{}".format(cuda_version))
           ,"cuda-cufft" : [ Package("cuda-cufft-{}".format(cuda_version)) , BuildPackage("cuda-cufft-dev-{}".format(cuda_version)) ]
           ,"cuda-curand" : [ Package("cuda-curand-{}".format(cuda_version)) , BuildPackage("cuda-curand-dev-{}".format(cuda_version)) ]
        }
        cuda_repo_base = "http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64"
        installer = [ InitCommand('apt-get update -y'),
                      InitCommand('apt-get install -y software-properties-common'),
                      InitCommand('apt-get install -y gnupg'),
                      InitCommand('apt-key adv --fetch-keys ' + cuda_repo_base + '/3bf863cc.pub'),
                      InitCommand('add-apt-repository "deb ' + cuda_repo_base + '/ /"'),
                      InitCommand('export DEBIAN_FRONTEND=noninteractive'),
                      InitCommand('apt-get update -y'),
                    ]
        super(UbuntuCudaPackageManager, self).__init__(packages, install=installer, name="CudaUbuntu")
