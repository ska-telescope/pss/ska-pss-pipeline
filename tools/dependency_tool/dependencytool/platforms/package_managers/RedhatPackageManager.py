from .YumPackageManager import YumPackageManager
from dependencytool.Package import Package, BuildPackage
from dependencytool.Commands import InitCommand

class RedhatPackageManager(YumPackageManager):
    """ defines the rpm pacakges avaialble in the standard redhat distribution
    """
    def __init__(self, other_packages={}, install=[], name="Yum"):
        packages = {
            # alphabetical order please
            "boost" : [ Package("boost"), BuildPackage("boost-devel") ]
           ,"cmake" : Package("cmake", attributes = { "cmake": "/usr/bin/cmake" })
           ,"cpack" : Package("rpm-build")
           ,"fftw" : [ Package("fftw"), BuildPackage("fftw-devel") ]
           ,"g++" : Package("gcc-c++")
           ,"make" : Package("make", attributes = { "make": "/usr/bin/make" } )
           ,"nasm" : Package("nasm", attributes = { "nasm": "/usr/bin/nasm" } )
           ,"python": Package("python3")
           ,"python-pip": Package("python3-pip")
        }
        packages.update(other_packages)
        super(RedhatPackageManager, self).__init__(packages, install, name)

class RedhatCudaPackageManager(YumPackageManager):
    def __init__(self, cuda_version):
        version_alt = str(cuda_version).replace(".", "-")
        packages = {
           "cuda" : Package("cuda-{}".format(version_alt))
           ,"cuda-runtime" : Package("cuda-runtime-{}".format(version_alt))
           ,"cuda-toolkit" : BuildPackage("cuda-toolkit-{}".format(version_alt))
           ,"cuda-cufft" : [ Package("cuda-cufft-{}".format(version_alt)) , BuildPackage("cuda-cufft-dev-{}".format(version_alt)) ]
           ,"cuda-curand" : [ Package("cuda-curand-{}".format(version_alt)) , BuildPackage("cuda-curand-dev-{}".format(version_alt)) ]
        }
        cuda_repo = "cuda-rhel7.repo"
        installer = [ InitCommand("yum-config-manager --add-repo https://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/" + cuda_repo)
                    ]
        super(RedhatCudaPackageManager, self).__init__(packages, install=installer, name="CudaRepo")

