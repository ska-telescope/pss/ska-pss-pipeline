from dependencytool.Platform import Platform
from dependencytool.PackageManager import ToolConfig, RepoPackage
from dependencytool.Commands import Command, InitCommand
from .package_managers.AptPackageManager import AptPackageManager
from .package_managers.DebianPackageManager import DebianPackageManager
from .package_managers.UbuntuPackageManager import UbuntuCudaPackageManager
from .package_managers.CudaRepository import CudaRepository

class Ubuntu(Platform):
    def __init__(self, version="latest", *args):
        super(Ubuntu, self).__init__("Ubuntu" + str(version), DebianPackageManager(),
                ToolConfig("docker", { "image": "ubuntu:" + str(version) } ), *args)

class Ubuntu18_04(Ubuntu):
    def __init__(self):
        super(Ubuntu18_04, self).__init__(18.04, AptPackageManager()
                                     , CudaRepository(UbuntuCudaPackageManager)
                                     )
