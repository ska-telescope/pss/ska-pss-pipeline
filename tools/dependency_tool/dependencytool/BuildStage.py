class BuildStage(object):
    """
        represent a stage in a build pipeline
    """
    def __init__(self, name, *commands):
        self._name = name
        self._commands = []
        for cmd in commands:
            self._commands.append(cmd)

    def __iter__(self):
        return self._commands.__iter__()

    def commands(self):
        return self._commands

    def name(self):
        return self._name
