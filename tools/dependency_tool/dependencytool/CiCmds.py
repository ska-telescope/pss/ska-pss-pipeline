from dependencytool.Platforms import Platforms
from dependencytool.SpinSpec import SpinSpec


class CiCmds(object):
    """
       Extracts information form the command line arguments
       Converting them into usable objects
         args    : dict of command line args
         project : the Project instance
    """
    def __init__(self, args, project):
        self._project = project
        self._args = args
        self._tool_args = {}

    def project(self):
        return self._project

    def args(self):
        return self._args

    """
       return a list of arguments to be associated with a specific tool
    """
    def tool_args(self, tool_name):
        arg_name = "tool_" + tool_name
        if not self._tool_args.get(arg_name):
            self._tool_args[arg_name] = []
            for arg in vars(self._args).get(arg_name, []):
                # strip off leading escape character inserted by arg parser
                self._tool_args[arg_name].append(arg[1:])
        return self._tool_args.get(arg_name)

    def executable_name(self):
        """
            return the name of the currently running executable
        """
        return self._args.prog

    def get_platform(self):
        """
            get the platform specified by the user
            If no specification is found will set the
            platform to match that of the local machine
        """
        platforms = Platforms()
        if self._args.platform is None:
            plat = platforms.local()
            if plat != None:
                return plat
            print("you must set a platform")
            return None
        return platforms.platform(self._args.platform.lower())

    def get_spin_spec(self):
        """
            get the spin specified by the user if any
            If no spin is specifed returns an empty SpinSpec object
        """
        if self._args.spin is None:
            spin_spec = SpinSpec()
        else:
            spin_spec = SpinSpec(self.spin())
        return spin_spec

    def get_spins(self):
        """
            get a list of spins corresponding to the user spin specification
        """

        spins = self._project.find_spins(self.get_spin_spec())
        if len(spins) == 0:
            raise NameError("unknown spin requested '" + self._args.spin + "'")
        return spins

    def get_spin_deps(self, filters = {}):
        """
            get a list of Dependency objects corresponding to
            the users specified spin (or default spin if none specified)
        """
        spins = self.get_spins()

        names = {}
        deps = []
        for spin in spins:
            for dep in spin.dependencies( filters ):
                if not dep.name() in names:
                    names[dep.name()] = True
                    deps.append(dep)
        return deps

    def spin(self):
        """
            The spin specification string supplied by the user
        """
        return self._args.spin

    def stage_filter(self):
        """
            The spin specification string supplied by the user
        """
        return self._args.stage_filter

    def build(self):
        """
            The build flag supplied by the user
        """
        return self._args.build

    def execute(self):
        return self._args.execute

    def dependencies_filenames_option(self):
        """
            return a string specifiying the dependecies options
        """
        if self._args.deps:
            return " --deps " + self._args.deps
        return ""
