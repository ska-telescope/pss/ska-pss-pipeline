from .BuildStage import BuildStage

class StageAll(object):
    """
        A group that contains all stages
    """
    def __init__(self):
        pass

    def init(self, stage_list):
        pass

    """
        copy constructor info only
    """
    def copy(self):
        return StageAll()

    def contains(self, name):
        return True

class StageExclude(object):
    """
        A group that contains all stages except that provided
    """
    def __init__(self, stage_name):
        self._name = stage_name

    def init(self, stage_list):
        pass

    """
        copy constructor info only
    """
    def copy(self):
        return StageExclude(self._name)

    def contains(self, name):
        if name == self._name:
            return False
        return True

class StageUpTo(object):
    """
        A set of stages up to, and including, the provided stage name
    """
    def __init__(self, stage_name):
        self._name = stage_name

    def init(self, stage_list):
        self._stages = stage_list
        self._index = get_stage_index(stage_list, self._name)

    """
        copy constructor info only
    """
    def copy(self):
        return StageUpTo(self._name)

    def contains(self, name):
        if self._index is None:
            raise RuntimeError("attempt to use unitialised object")
        return not get_stage_index(self._stages, name) > self._index

class StageAfter(object):
    """
        A set of stages after the provided stage name
    """
    def __init__(self, stage_name):
        self._name = stage_name

    def init(self, stage_list):
        self._stages = stage_list
        self._index = get_stage_index(stage_list, self._name)

    """
        copy constructor info only
    """
    def copy(self):
        return StageAfter(self._name)

    def contains(self, name):
        if not self._index:
            raise RuntimeError("attempt to use unitialised object")
        return get_stage_index(self.stages, name) > self._index

class StageGroup(object):
    """
        A group that contains a set of stage names, and stage groups
    """
    def __init__(self, config_list):
        self._config = config_list

    """
        initialise the group with the complete list of ordered available stages
    """
    def init(self, stage_list):
        for stage_gp in self._config:
            if isinstance(stage_gp, str):
                continue
            stage_gp.init(stage_list)

    """
        copy constructor info only ( i.e. pre init() call )
    """
    def copy(self):
        other = []
        for gp in self._config:
            if isinstance(gp, str):
                other.append(gp)
            else:
                other.append(gp.copy())
        return StageGroup(other)

    def contains(self, name):
        if isinstance(name, BuildStage):
            name = name.name()

        self._config
        for stage_gp in self._config:
            if isinstance(stage_gp, str):
                if name == stage_gp:
                    return True
                return False
            if stage_gp.contains(name):
                return True
        return False

class StageFilter(object):
    def __init__(self, stage_group, stage_list):
        assert isinstance(stage_list, list)
        if not is_stage_group(stage_group):
            raise TypeError("expecting a stage group type, got " + str(type(stage_group)))

        # make a copy of the stage group
        self._group = stage_group.copy()
        self._group.init(stage_list)

    def contains(self, stage):
        if isinstance(stage, BuildStage):
            stage = stage.name()
        return self._group.contains(stage)

def get_stage_index(stage_list, stage_name):
    """
        fine the named stage in a list of objects that have a
        name() method
    """
    index = 0
    for stage in stage_list:
        if stage.name() == stage_name:
            return index
        index = index + 1
    raise RuntimeError("requested a non exisitng stage name: "
                       + stage_name)


def is_stage_group(object):
    """
        return True if the type of the object passed is one of the above Stage group types i.e:
            StageGroup
            StageAll
            StageAfter
            StageUpTo
        Otherwise returns False
    """
    if isinstance(object, StageAll):
        return True
    if isinstance(object, StageUpTo):
        return True
    if isinstance(object, StageAfter):
        return True
    if isinstance(object, StageGroup):
        return True
    return False
