from .Version import Version

class Package(object):
    """ Represents a package associated with some specific installable software
        Each pacakge will have a name, an (optional) version
        and a series of attributes that describe the contents of the package
        e.g. the executables provided and the location of that executable on the system
             after the package has been installed
    """
    def __init__(self, name="", version=Version(), attributes = {}):
        self._name=name
        self._version=version
        if not isinstance(self._version, Version):
            raise TypeError("Package must be provided with a Version object for version info")
        self._attrib = attributes
        if self._attrib.get("spin") is None:
                self._attrib["spin"] = "runtime"

    def name(self):
        return self._name

    def attribute(self, name):
        return self._attrib.get(name)

    def version(self):
        return self._version

    def __str__(self):
        return self.name()

    def __eq__(self, other):
        return (self.name() == other.name()) and (self._version.matches(other.version()))

    def __ne__(self, other):
        return not (self == other)

class BuildPackage(Package):
    """ A developer spin version of a software Package
        Typically this will add features to the base Package such as header files
        that allow developers to link to the underlying library
    """
    def __init__(self, name="", version=Version(), attributes = {}):
        attributes["spin"] = "build"
        super(BuildPackage, self).__init__(name, version, attributes)
        pass


