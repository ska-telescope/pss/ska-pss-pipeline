""" Unit tests for the classes in Commands.py"""
from dependencytool.Commands import Command
from dependencytool.Commands import InitCommand
from dependencytool.Commands import Procedure 

def test_command():
    cmdstr = "echo Hello"
    cmd = Command(cmdstr);
    assert cmd.name() == ""
    assert str(cmd) == cmdstr

def test_init_command():
    cmdstr = "echo HelloInit"
    cmd = InitCommand(cmdstr);
    assert cmd.name() == ""
    assert str(cmd) == cmdstr

def test_procedure():
    proc = Procedure("my_proc")
    assert len(proc) == 0
    proc.add(Command("1"))
    assert len(proc) == 1
    assert len(proc.commands()) == len(proc)
    proc.add(Command("2"))
    assert len(proc) == 2
    proc.add(InitCommand("Init1"))
    assert len(proc.commands()) == len(proc)
    assert len(proc) == 3
    proc.add(InitCommand("Init2"))
    assert len(proc.commands()) == len(proc)
    assert len(proc) == 4
    proc.add(Command("3"))
    assert len(proc) == 5
    assert len(proc.commands()) == len(proc)

    # -- verify InitCommands have floated to the top
    cmds = proc.commands()
    assert cmds[0] == Command("Init1")
    assert cmds[1] == Command("Init2")
    assert cmds[2] == Command("1")
    assert cmds[3] == Command("2")
    assert cmds[4] == Command("3")

def test_procedure_add_to_procedure():
    proc_a = Procedure("my_proc")
    proc_b = Procedure("my_sub_proc")
    proc_b.add(Command("Sub 1"))
    proc_b.add(InitCommand("Sub Init1"))

    proc_a.add(proc_b)
    assert len(proc_a) == 2
    assert len(proc_a.commands()) == len(proc_a)
    cmds = proc_a.commands()
    assert cmds[0] == InitCommand("Sub Init1")
    assert cmds[1] == Command("Sub 1")

def test_procedure_in_list_add_to_procedure():
    proc_a = Procedure("my_proc")
    proc_b = Procedure("my_sub_proc")
    proc_b.add(Command("Sub 1"))
    proc_b.add(InitCommand("Sub Init1"))

    proc_a.add([proc_b, Command("1")])
    assert len(proc_a) == 3
    assert len(proc_a.commands()) == len(proc_a)
    cmds = proc_a.commands()
    assert cmds[0] == InitCommand("Sub Init1")
    assert cmds[1] == Command("Sub 1")
    assert cmds[2] == Command("1")
