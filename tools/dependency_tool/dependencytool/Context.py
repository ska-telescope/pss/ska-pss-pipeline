from .CiCmds import CiCmds

class Context(object):
    def __init__(self, cmds):
        """
            object to define a common  context in which different objects should operate
        """
        if not isinstance(cmds, CiCmds):
            raise TypeError("expecting a CiCmds object")
        self._cmds = cmds

    def spin_spec(self):
        return self._cmds.get_spin_spec()

    def args(self):
        """
            return the CiCmds object
        """
        return self._cmds

    def tool_args(self, tool_name):
        """
           return a list of arguments to be associated with a specific tool
        """
        return self._cmds.tool_args(tool_name)

    def spin_spec(self):
        """
            return the user defined SpinSpec or None
        """
        if self._cmds.spin():
            return self._cmds.get_spin_spec()
        return None

    def spins(self):
        """
            return the user defined Spins
        """
        return self._cmds.get_spins()
