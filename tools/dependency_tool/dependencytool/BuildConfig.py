from dependencytool.Dependency import Dependency
from dependencytool.Dependencies import Dependencies, Spin

class BuildConfig:
    """ Describes a set of dependecies for which the project can be configured
        for. for example, different build tool chains, or dependency versions, or optional deps
    """
    def __init__(self, name, spin):
        self._name = name
        self._spin = spin
        self._deps = spin.dependencies()

        if self._deps is None:
            raise RuntimeError("expecting a list of dependencies but got None")

    def name(self):
        return self._name

    def spin(self):
        return self._spin

    def dependencies(self):
        return self._deps

    def add_dependencies(self, dependency_list):
        if not isinstance(dependency_list, list):
            raise TypeError("expecting a list of dependencies but got " + str(type(dependency_list)))
        for dep in dependency_list:
            self._deps.append(dep)

    def executable(self, dependency, executable_name, platform):
        """ gets the executable from the specifed dependency package info
            available on the secific platform
        """
        if not isinstance(dependency, Dependency):
            raise TypeError("BuildConfig::executable expecting a Dependency object")

        # -- find the relevant dependency in this configuration
        for dep in self._deps:
            if dep.name() == dependency.name():
                return platform.executable(executable_name, dep)

        # -- no dep specified so just ask the platform
        raise RuntimeError("BuildConfig::execute error: required dependency \"" + str(dependency.name()) + "\" has not been specified in this project. Please add it and try again.")

def build_config_from_spin(spin, name_prefix=""):
    config_name = name_prefix
    if(spin.name()!=""):
        config_name = config_name + "_" + spin.name(seperator="_")
    return BuildConfig(config_name, spin)

def build_configurations(name_prefix, deps):
    if not isinstance(deps, Dependencies):
            raise TypeError("expecting a Dependencies object")

    configs = []
    for spin in deps.spins():
        configs.append(build_config_from_spin(spin, name_prefix))
    return configs

