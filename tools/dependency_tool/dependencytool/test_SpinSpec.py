""" Unit tests for the SpinSpec class"""
from dependencytool.SpinSpec import SpinSpec
from dependencytool.Dependency import Dependency

def test_dependecies_empty_string():
    spin_spec = SpinSpec()
    assert len(spin_spec.dependencies()) == 0

def test_None_comparison():
    """
        Given a spin_spec with no dependecies
        When compared to None
        Then shall be equal
    """
    spin_spec = SpinSpec()
    assert spin_spec == None
    """
        Given the default spin_spec
        When compared to None
        Then shall not be equal
    """
    spin_spec2 = SpinSpec("default")
    assert spin_spec2 != None

def test_dependecies():
    spin_spec = SpinSpec("a,b")
    deps = spin_spec.dependencies()
    assert len(deps) == 2

def test_matches():
    spin_spec = SpinSpec("a,b")
    assert spin_spec.matches(Dependency("a"))
    assert spin_spec.matches(Dependency("b"))
    assert not spin_spec.matches(Dependency("c"))

